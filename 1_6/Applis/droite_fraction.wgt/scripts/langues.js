var version = "?", auteur = "François Le Cléac'h", lien_auteur= "https://openedu.fr/";
if(window.sankore)
{ // Récupération des données depuis le fichier config.xml
	version = window.widget.version;
	auteur = window.widget.author;
	lien_auteur = window.widget.authorHref;
} 
var sankoreLang = {
"fr":{
	Reglages:"Réglages",
	Longueur:"Longueur",
	Ligne:"Ligne",
	Unite:"Unité",
	Origine:"Origine",
	Decalage:"Décalage",
	Taille:"Taille",
	Denominateur:"Dénominateur",
	Fractions:"Fractions",
	Epaisseur:"Épaisseur",
	Hauteur:"Hauteur",
	Infos:"À propos",
	Txt_infos:"<h1 style='text-align:center;'>Droite et fractions</h1>"
	+"<h2>Application pour Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> Solar Linear Icons https://www.svgrepo.com/<br><h3>Bibliothèques :</h3><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"en":{
	Reglages:"Preferences",
	Longueur:"Length",
	Ligne:"Line",
	Unite:"Unit",
	Graduations:"Graduations",
	Sous_Graduations:"Under Graduations",
	Numerotation:"Numbering",
	Origine:"Origin",
	Decalage:"Shift",
	Taille:"Size",
	Nombre:"Number",
	Fractions:"Fractions",
	Epaisseur:"Thickness",
	Hauteur:"Height",
	Infos:"About",
	Txt_infos:"<h1 style='text-align:center;'>Line and fractions</h1>"
	+"<h2>Application for Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><b>Icons :</b> Solar Linear Icons https://www.svgrepo.com/<br><b>Libary :</b><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"de":{
	Reglages:"Einstellungen",
	Longueur:"Länge",
	Ligne:"Linie",
	Unite:"Einheit",
	Graduations:"Einteilungen",
	Sous_Graduations:"Unterteilungen",
	Numerotation:"Nummerierung",
	Origine:"Ursprung",
	Decalage:"Verschiebung",
	Taille:"Größe",
	Nombre:"Nummer",
	Fractions:"Brüche",
	Epaisseur:"Dicke",
	Hauteur:"Höhe",
	Infos:"Über",
	Txt_infos:"<h1 style='text-align:center;'>Linie und Brüchen</h1>"
	+"<h2>App für Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><b>Icons :</b> Solar Linear Icons https://www.svgrepo.com/<br><b>Bibliothek :</b><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
}
};
