var version = "?", auteur = "François Le Cléac'h", lien_auteur= "https://openedu.fr/";
if(window.sankore)
{ // Récupération des données depuis le fichier config.xml
	version = window.widget.version;
	auteur = window.widget.author;
	lien_auteur = window.widget.authorHref;
} 
var sankoreLang = {
"fr":{
	Infos:"À propos",
	Txt_infos:"<h1 style='text-align:center;'>Fraction</h1>"
	+"<h2>Application pour Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> Solar Linear Icons https://www.svgrepo.com/<br><h3>Bibliothèques :</h3><ul><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"en":{
	Infos:"About",
	Txt_infos:"<h1 style='text-align:center;'>Fraction</h1>"
	+"<h2>Application for Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><b>Icons :</b> Solar Linear Icons https://www.svgrepo.com/<br><b>Libary :</b><ul><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"de":{
	Infos:"Über",
	Txt_infos:"<h1 style='text-align:center;'>Bruch</h1>"
	+"<h2>App für Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><b>Icons :</b> Solar Linear Icons https://www.svgrepo.com/<br><b>Bibliothek :</b><ul><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
}
};
