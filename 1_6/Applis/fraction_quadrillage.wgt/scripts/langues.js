var version = "?", auteur = "François Le Cléac'h", lien_auteur= "https://openedu.fr/";
if(window.sankore)
{ // Récupération des données depuis le fichier config.xml
	version = window.widget.version;
	auteur = window.widget.author;
	lien_auteur = window.widget.authorHref;
} 
var sankoreLang = {
"fr":{
	Reglages:"Paramètres",
	Grille:"Grille",
	Lignes:"Lignes",
	Colonnes:"Colonnes",
	Case:"Case",
	Hauteur:"Hauteur",
	Largeur:"Largeur",
	Epaisseur:"Épaisseur",
	Fond:"Fond",
	Etiquettes:"Étiquettes",
	Taille:"Taille",
	Infos:"À propos",
	Txt_infos:"<h1 style='text-align:center;'>Fraction de quadrillage</h1>"
	+"<h2>Application pour Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> Solar Linear Icons https://www.svgrepo.com/<br><h3>Bibliothèques :</h3><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"en":{
	Reglages:"Preferences",
	Grille:"Grid",
	Lignes:"Lines",
	Colonnes:"Columns",
	Case:"Box",
	Hauteur:"Height",
	Largeur:"Width",
	Epaisseur:"Thickness",
	Fond:"Background",
	Etiquettes:"Labels",
	Taille:"Size",
	Infos:"About",
	Txt_infos:"<h1 style='text-align:center;'>Grid fraction</h1>"
	+"<h2>Application for Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><b>Icons :</b> Solar Linear Icons https://www.svgrepo.com/<br><b>Libary </b><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"de":{
	Reglages:"Einstellungen",
	Grille:"Raster",
	Lignes:"Linien",
	Colonnes:"Spalten",
	Case:"Feld",
	Hauteur:"Höhe",
	Largeur:"Breite",
	Epaisseur:"Dicke",
	Fond:"Hintergrund",
	Etiquettes:"Etiketten",
	Taille:"Größe",
	Infos:"Über",
	Txt_infos:"<h1 style='text-align:center;'>Fraktion von Gitternetzlinien</h1>"
	+"<h2>App für Open-Sankor&eacute; / OpenBoard</h2>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><b>Icons :</b> Solar Linear Icons https://www.svgrepo.com/<br><b>Bibliothek :</b><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
}
};
