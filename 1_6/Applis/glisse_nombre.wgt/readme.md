
# Glisse Nombre Cycle 3
Application pour Openboard

Illustrer la multiplication ou la division d'un nombre entier ou décimal par 10, 100 ou 1000.
# Auteur
François Le Cléac'h https://openedu.fr
Avril 2024
# Licence 
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 

# Versions
## 1.0
* Création d'une bande glissante
## 1.1
* Modification de la CSS
	* Forme des cases des chiffres
	* Mise en forme du tableau de numération
	* Réglage du pas de décallage
	* Ajout de la virgule et de la partie décimale
## 1.2
* Création des boutons Multiplier et diviser
## 1.3
* Mise en forme des boutons
## 1.4
* Ajout des boutons 10, 100, 1 000 sous celui de la multiplication et la division
## 1.5
* Ajout des boutons + et - sur chaque case permettant d'incrémenter les chiffres
## 1.6
* Limitation de chaque case à 1 seul chiffre
* Passage du 9 au 0 puis à la case vide dans l'incrémentation
* Passage de 0 à la case vide puis au 9 dans la décrémentation
## 2.0
* Création du bouton "recommencer" replacçant la bande dans sa position initiale
* Création du bouton "effacer", remettant le tableau à l'état vierge
## 2.1
* Ajout d'un menu "Aide"
## 2.5
* Correction des problèmes d'affichage (sous Chrome)
## 3.0
* Réécriture intégrale pour une compatibilité **OpenBoard 1.5**
* Refonte graphique
* Ajout d'une zone texte contenant le nombre
* Fonctionnement type compteur
	* Passage au nombre suivant après 9
	* Passage au nombre précédent avant le 0
	* Ajout des 0 manquant
	* Suppression des 0 inutiles
* Simplification du menu
* Ajout d'un mode sombre
* Mise en évidence des unités (encadré rouge)
* Internationalisation de l'application (**scripts/langues.js**)
    * Français
    * Allemand
    * Anglais
* Normalisation du fichier **config.xml**
    * identifiant unique de l'application pour les mises à jours ultérieures
* Adaptation pour un fonctionnement Hors Openboard (navigateur simple)
# Sources
## Icones :
* Solar Linear Icons https://www.svgrepo.com/
## Bibliothèques :
* Raphael JS https://dmitrybaranovskiy.github.io/raphael/
* Laktek colorPicker https://github.com/laktek/really-simple-color-picker
* Jquery 3.7.1 https://jquery.com/