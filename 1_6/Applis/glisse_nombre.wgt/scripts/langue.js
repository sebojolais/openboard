var version = "?", auteur = "François Le Cléac'h", lien_auteur= "https://openedu.fr/";
if(window.sankore)
{ // Récupération des données depuis le fichier config.xml
	version = window.widget.version;
	auteur = window.widget.author;
	lien_auteur = window.widget.authorHref;
}
var sankoreLang = {
"fr":{
	milliards:"Milliards",
	millions:"Millions",
	mille:"Mille",
	unites:"Unités",
	dixiemes:"dixièmes",
	centiemes:"centièmes",
	milliemes:"millièmes",
	dixmilliemes:"dix-millièmes",
	c:"c",
	d:"d",
	u:"u",
	Separateur_decimal:",",
	Infos:"Glisse Nombre Cycle 3",
	Txt_infos:"<h1>Application pour OpenBoard</h1>"
	+"Adaptation numérique de l'outil présenté sur <a href='http://cache.media.education.gouv.fr/file/Fractions_et_decimaux/42/2/RA16_C3_MATH_frac_dec_annexe_4_673422.pdf' target='blank'>Eduscol Cycle 3</a><br>"
	+"Illustration de la multiplication ou la division d'un nombre par 10, 100, 1 000...<br>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> Solar Linear Icons https://www.svgrepo.com/<br><h3>Bibliothèques :</h3><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
},
"en":{
	milliards:"Billions",
	millions:"Millions",
	mille:"Thousands",
	unites:"Units",
	dixiemes:"tenths",
	centiemes:"hundredths",
	milliemes:"thousandths",
	dixmilliemes:"ten-thousandths",
	c:"h",
	d:"t",
	u:"u",
	Separateur_decimal:",",
	Infos:"Slide Number",
	Txt_infos:"<h1>Application for OpenBoard</h1>"
	+"Digital Adaptation of the tool presented on <a href='http://cache.media.education.gouv.fr/file/Fractions_et_decimaux/42/2/RA16_C3_MATH_frac_dec_annexe_4_673422.pdf' target='blank'>Eduscol Cycle 3</a><br>"
	+"Illustration of multiplication or division by par 10, 100, 1 000...<br>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> Solar Linear Icons https://www.svgrepo.com/<br><h3>Libary :</h3><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"

	},
"de":{
	milliards:"Milliarden",
	millions:"Millionen",
	mille:"Tausenden",
	unites:"Einheiten",
	dixiemes:"zehnte",
	centiemes:"hundertstel",
	milliemes:"tausendstel",
	dixmilliemes:"zehntausendstel",
	c:"h",
	d:"z",
	u:"e",
	Separateur_decimal:",",
	Infos:"Schiebt Zahl",
	Txt_infos:"<h1>App für OpenBoard</h1>"
	+"Digitale Anpassung des auf <a href='http://cache.media.education.gouv.fr/file/Fractions_et_decimaux/42/2/RA16_C3_MATH_frac_dec_annexe_4_673422.pdf' target='blank'>Eduscol Cycle 3</a> vorgestellten Werkzeug<br>"
	+"Abbildung der Multiplikation oder Division einer Zahl durch 10, 100, 1.000...<br>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> Solar Linear Icons https://www.svgrepo.com/<br><h3>Bibliothek :</h3><ul><li>Raphael JS https://dmitrybaranovskiy.github.io/raphael/</li><li>Laktek colorPicker https://github.com/laktek/really-simple-color-picker</li><li>Jquery 3.7.1 https://jquery.com/</li></ul></div>"
	}
};