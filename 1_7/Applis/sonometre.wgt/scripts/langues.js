var version = "1.0", auteur = "François Le Cléac'h", lien_auteur= "https://openedu.fr/";
if(window.sankore)
{ // Récupération des données depuis le fichier config.xml
	version = window.widget.version;
	auteur = window.widget.author;
	lien_auteur = window.widget.authorHref;
} 
var sankoreLang = {
"fr":{
	titre01:"Sensibilité",
	titre02:"Taille",
	titre03:"Style",
	titre04:"Rond",
	titre05:"Carré",
	titre06:"À propos",
	Txt_infos:"<h1 style='text-align:center;'>Sonomètre</h1>"
	+"<h2>Application pour Open-Sankor&eacute; / OpenBoard</h2>"
	+"<p>Mesure du niveau sonore dans la classe.</p>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> <ul><li>VU Alex Wendpap https://www.flaticon.com/fr/</li><li>Solar Linear Icons https://www.svgrepo.com/</li></div>"
},
"en":{	
	titre01:"Sensitivity",
	titre02:"Size",
	titre03:"Style",
	titre04:"Round",
	titre05:"Square",
	titre06:"About",
	Txt_infos:"<h1 style='text-align:center;'>Sound level meter</h1>"
	+"<h2>Application for Open-Sankor&eacute; / OpenBoard</h2>"
	+"<p>Noise level measurement in the classroom.</p>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
	+"<div class='sources'><h3>Icones :</h3> <ul><li>VU Alex Wendpap https://www.flaticon.com/fr/</li><li>Solar Linear Icons https://www.svgrepo.com/</li></div>"
},
"de":{
	titre01:"Empfindlichkeit",
	titre02:"Größe",
	titre03:"Stil",
	titre04:"Runde",
	titre05:"Quadrat",
	titre06:"Über",
	Txt_infos:"<h1 style='text-align:center;'>Sound level meter</h1>"
	+"<h2>Application for Open-Sankor&eacute; / OpenBoard</h2>"
	+"<p>Messung des Geräuschpegels im Klassenzimmer.</p>"
	+"<div class='version'>Version "+version+"</div>"
	+"<div class='auteur'>"+auteur+"</div>"
	+"<div style='text-align:center;'><button class='ccbyncsa'></button><span class='site_web'><a href='"+lien_auteur+"'>"+lien_auteur+"</a></span></div>"
+"<div class='sources'><h3>Icones :</h3> <ul><li>VU Alex Wendpap https://www.flaticon.com/fr/</li><li>Solar Linear Icons https://www.svgrepo.com/</li></div>"
}
};
