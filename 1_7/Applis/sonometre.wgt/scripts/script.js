let langue_defaut='fr', syslang=langue_defaut;// Français par défaut
let classe="rond", mode="param";  // Apparence du sonomètre, mode param: menu des réglages visibles
let taille_min=150,taille_max=500,taille_pas=50, taille_sonometre=250; // Dimension du sonomètre
let gain_min=1,gain_max=100,gain_pas=1, sensibilite=10;// sensibilité du sonomètre, min à 1 pour ne pas couper le micro
const openboard=Boolean(window.sankore),faux=Boolean(""),vrai=!faux;
const ControleTaille = document.getElementById('taille');
const ControleGain = document.getElementById('gain');
const NiveauSon = document.getElementById('niveau-sonore');
const Forme = document.getElementsByName('forme');
const Fermer = document.getElementById('fermer');
const FermerAide = document.getElementById('fermer_aide');
const Menu = document.getElementById('menu'); //Boite de dialogue des paramètres
const btnAide = document.getElementById('aide'); //Bouton d'aide
const Infos = document.getElementById('infos');//Boite de dialogue de l'aide 
const TexteInfos = document.getElementById('txt_infos');//Contenu de l'aide
const btnReglages = document.getElementById('reglages');//Bouton des paramètres
async function init(){
	init_lang();	
	// Arrivée sur le widget 
	if (openboard){
		window.widget.onenter.connect(() => {
			//Montre le bouton d'accès aux réglages en fonction du mode d'affichage
			if (mode!="param"){montre(btnReglages);}
			});
		// Sortie du Widget
		window.widget.onleave.connect(() => {
			//Sauvegarde des paramètres au format 'chaine de caractères'
			window.sankore.setPreference('sensibilite', sensibilite);
			window.sankore.setPreference('taille_sonometre', taille_sonometre);
			window.sankore.setPreference('classe', classe);
			window.sankore.setPreference('mode', mode);
			cache(btnReglages);
		});
		// Quand on revient sur la page / copie le widget, on récupère les paramètres stockés
		if (await window.sankore.async.preference('sensibilite')) {// Récupération des paramètres sauvegardés s'ils existent (retour sur la page)
			sensibilite=parseInt(await window.sankore.async.preference('sensibilite'));
			taille_sonometre=parseInt(await window.sankore.async.preference('taille_sonometre'));
			classe=(await window.sankore.async.preference('classe'));
			mode=(await window.sankore.async.preference('mode'));
		} 
	}
	// Initialisation des paramètres
	// Sonomètre
	// Choix de la forme du Sonomètre
	document.getElementById(classe).checked = true; // Selectionne l'apparence de base
	NiveauSon.classList.add(classe);// Mise en forme
	redimensionne_sonometre();// Mise à l'échelle
	// Réglage de taille
	ControleTaille.step=taille_pas; // Incrémentation
	ControleTaille.min=taille_min; // Taille minimale
	ControleTaille.max=taille_max; // Taille maximale
	ControleTaille.value=taille_sonometre; // Règle la taille initiale
	// Réglage de la sensibilité
	ControleGain.step=gain_pas; // Incrémentation
	ControleGain.min=gain_min; // Sensibilité minimale
	ControleGain.max=gain_max; // Sensibilité maximale
	ControleGain.value=sensibilite; // Règle la sensibilité initial
	affichage(mode);//Mode d'affichage
	// Gestion des événements
	// Bouton Réglages
	btnReglages.addEventListener("click", function() {
		cache(this);//Cache le bouton
		montre(Menu);//Affice le menu
		mode="param";//Actualisation du mode d'affichage
	});
	// Bouton Aide
	btnAide.addEventListener("click", function() {
		montre(Infos);//Affiche l'aide
		});
	// Bouton Fermer_Aide
	FermerAide.addEventListener("click", function() {
		cache(Infos);//Cache l'aide
		});
	// Bouton Fermer
	Fermer.addEventListener("click", function() {
		montre(btnReglages);//Affiche le bouton d'accès aux réglages
		cache(Menu);//Cache les réglages
		mode="";//Actualisation du mode d'affichage
		});
	// Curseur de la taille
	ControleTaille.addEventListener('input', (event) => {
		taille_sonometre=event.target.value;// Mise à jour de la taille
		redimensionne_sonometre();
		});
	// Fonction des boutons radios 
	for (var i = 0; i < Forme.length; i++) {
		Forme[i].onclick = function() {
			NiveauSon.classList.remove(classe); // Suppression de la forme actuelle
			classe=this.value; // Mémorisation de la nouvelle forme 
			NiveauSon.classList.add(classe); // Nouvelle mise en forme
		}
	}
	Micro();//Accès au micro
}
function init_lang(){
	//Détection de la langue
	try{
		syslang = sankore.locale().substr(0,2);
        } catch(e){
		syslang = langue_defaut;
	}
	// Chargement du fichier de langue
	sankoreLang[syslang].search;
	// Traduction de l'interface
	document.getElementById('lbl_01').innerHTML=sankoreLang[syslang].titre01;
	document.getElementById('lbl_02').innerHTML=sankoreLang[syslang].titre02;
	document.getElementById('lbl_03').innerHTML=sankoreLang[syslang].titre03;
	document.getElementById('lbl_04').innerHTML=sankoreLang[syslang].titre04;
	document.getElementById('lbl_05').innerHTML=sankoreLang[syslang].titre05;
	// Mise à jour de la boîte de dialogue des informations
	TexteInfos.innerHTML=sankoreLang[syslang].Txt_infos;
	}
function affichage(mode){
	if (mode=="param"){ //Menu affiché
		cache(btnReglages);// Cache le bouton des paramètres
		montre(Menu); // Affiche la boite de dialogue des paramètres
		}else{
		montre(btnReglages);// Affiche le bouton des paramètres
		cache(Menu);// Cache la boite de dialogue des paramètres
		}
	}
function redimensionne_sonometre(){
	NiveauSon.style.width= taille_sonometre+'px'; 
	NiveauSon.style.height= taille_sonometre+'px';
	NiveauSon.style.fontSize = (taille_sonometre/8)+"px"; // Taille de la police proportionnelle à la dimension du sonomètre
	}
function cache(element){
	element.style.display="none";
	}
function montre(element){
	element.style.display="block";
	}
async function Micro() {
	// Demande la permission d'utiliser le microphone
	const flux = await navigator.mediaDevices.getUserMedia({ audio: true });
	const audio = new (window.AudioContext || window.webkitAudioContext)();
	const analyseur = audio.createAnalyser();
	const micro = audio.createMediaStreamSource(flux);
	const GainSonore = audio.createGain();
	analyseur.smoothingTimeConstant = 0.8;
	analyseur.fftSize = 1024;
	micro.connect(GainSonore);
	GainSonore.connect(analyseur);
	let array = new Uint8Array(analyseur.frequencyBinCount);
	traitement(array, analyseur)
	// Curseur du gain
	GainSonore.gain.value =sensibilite;//Réglage de la sensibilité
	ControleGain.addEventListener('input', (event) => {sensibilite=event.target.value;GainSonore.gain.value =sensibilite});
}
async function traitement(array, analyseur) {
	analyseur.getByteFrequencyData(array);
	let moyenne = array.reduce((sum, value) => sum + value, 0) / array.length;
	// Change la couleur en fonction du volume
	let volume = Math.min(1, moyenne / 128); // Normaliser le volume de 0 à 1
	let rouge = Math.min(255, volume * 510); // Composant rouge : Double le volume pour aller de vert à rouge
	let vert = Math.min(255, (1-volume)*510); // Composante verte : Inverse le volume pour le vert
	let bleu = 0; // Pas de composante bleue
	NiveauSon.style.backgroundColor = `rgb(${rouge}, ${vert}, ${bleu})`;
	NiveauSon.textContent = `Volume: ${Math.round(volume * 100)}%`;
	setTimeout(function() {traitement(array, analyseur);}, 100);
}
